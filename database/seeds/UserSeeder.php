<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Jose Enrique Reyes B',
            'email' => 'admin@email.com',
            'email_verified_at'=> new datetime,
            'password'=> bcrypt('12345678')
        ]);
    }
}
