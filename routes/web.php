<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::view('/loginAdmin', 'auth.loginAdmin')->name('loginAdmin');

//----------------------------Pagina de inicio del admin --------------------------------------
Route::get('/home/admin', 'Admin\DashboardAdminController@index')->name('admin.dashboard');

